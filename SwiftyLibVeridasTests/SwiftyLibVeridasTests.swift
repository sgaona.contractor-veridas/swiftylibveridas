//
//  SwiftyLibTestsVeridas.swift
//

import XCTest
@testable import SwiftyLibVeridas

class SwiftyLibVeridasTests: XCTestCase {
    
    var swiftyLibVeridas: SwiftyLibVeridas!

    override func setUp() {
        swiftyLibVeridas = SwiftyLibVeridas()
    }

    func testAdd() {
        XCTAssertEqual(swiftyLibVeridas.add(a: 1, b: 1), 2)
    }
    
    func testSub() {
        XCTAssertEqual(swiftyLibVeridas.sub(a: 2, b: 1), 1)
    }

}
