//
//  SwiftyLibExamplesApp.swift
//  SwiftyLibExamples
//
//  Created by itadmin on 25/4/24.
//

import SwiftUI

@main
struct SwiftyLibExamplesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
