Pod::Spec.new do |spec|
  spec.name         = "SwiftyLibVeridas"
  spec.version      = "0.0.2"
  spec.summary      = "A CocoaPods library written in Swift"

  spec.description  = <<-DESC
This CocoaPods library helps you perform calculations.
                   DESC

  spec.homepage     = "https://gitlab.com/sgaona.contractor-veridas/SwiftyLibVeridas"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Veridas" => "sgaona.contractor@veridas.com" }

  spec.ios.deployment_target = "12.1"
  spec.swift_version = "5.0"

  spec.source        = { :git => "https://gitlab.com/sgaona.contractor-veridas/SwiftyLibVeridas.git", :tag => "#{spec.version}" }
  spec.source_files  = "SwiftyLibVeridas/**/*.{h,m,swift}"
end

