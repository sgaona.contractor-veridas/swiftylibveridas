//
//  SwiftyLibVeridas.swift
//

public final class SwiftyLibVeridas {

    let name = "SwiftyLibVeridas"
    
    public func add(a: Int, b: Int) -> Int {
        return a + b
    }
    
    public func sub(a: Int, b: Int) -> Int {
        return a - b
    }
    
}
